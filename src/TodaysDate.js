//src/TodaysDate.js
import React from "react";
import Timer from 'react-compound-timer';

const TodaysDate = () => {
    return (
        <div>
            <p>{`Today's date is ${Date()}`}</p>
            <Timer
                key='timer-comppound'
                initialTime={10000}
                direction='backward'
            >
                {({ start, resume, pause, stop, reset, timerState }) => (
                    <React.Fragment>
                        <div>
                            <Timer.Days /> days
                                    <Timer.Hours /> hours
                                    <Timer.Minutes /> minutes
                                    <Timer.Seconds /> seconds
                                  </div>
                    </React.Fragment>
                )}
            </Timer>
        </div>)
};
export default TodaysDate;

